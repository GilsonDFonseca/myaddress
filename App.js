import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import axios from 'axios';


export default function App () {
  const [cep, setCep] = useState("");
  const [verified, setVerified] = useState(false);
  const [message, setMessage] = useState("");
  const [address, setAddress] = useState(null);
  const MAX_LENGTH = 8;
  const api = axios.create({
    baseURL: 'https://viacep.com.br/ws/',
  });



  function searchCep () {
    api.get(`${cep}/json/`)
      .then(function (response) {
        setCep("");
        setVerified(false);
        setAddress(response.data)
      })
      .catch(function (error) {
        setMessage("Ocorreu um erro");
      });
  }

  function handleChange (text) {
    setCep(text);
    if (!/^\d+$/gi.test(text)) {
      setMessage("Por favor, apenas números");
    } else {
      setMessage("");
      if (text.length === MAX_LENGTH) {
        setVerified(true);
      } else {
        setVerified(false);
      }
    }
  }

  console.log(address)

  return (
    <View style={styles.container}>
      <Text style={styles.text}>{"Favor preencha o campo abaixo com o CEP (apenas números)"}</Text>
      <TextInput
        placeholder="CEP"
        maxLength={MAX_LENGTH}
        style={styles.input}
        onChangeText={handleChange}
        value={cep}
      />
      <Text style={styles.hiddenText}>{message}</Text>
      <Button
        disabled={!verified}
        color="#00695C"
        onPress={searchCep}
        title="Buscar"
        accessibilityLabel="Busque pelo CEP digitado"
      />
      <div style={{ marginTop: 24}}>
        <div>
          <Text style={styles.text}>
            {address ? address.data ? address.data.erro : address.bairro + " - " + address.logradouro : ""}
          </Text>
        </div>
        <div>
          <Text style={styles.text}>
            {address ? address.localidade + " - " + address.uf : ""}
          </Text>
        </div>
      </div>

    </View >
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    marginTop: 40
  },



  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    alignItems: 'center',

  },

  text: {
    color: '#424242',
    marginBottom: 24,
    fontFamily: 'Roboto Slab',
    fontSize: 24,
  },

  hiddenText: {
    color: 'green',
    margin: 12
  },

  responseDiv: {
    marginTop: 24
  }


});
